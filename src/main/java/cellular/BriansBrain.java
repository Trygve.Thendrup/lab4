package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {
	
	
	IGrid currentGeneration;

	public BriansBrain(int i, int j) {
		currentGeneration = new CellGrid(i, j, CellState.DEAD);
		initializeCells();
	}

	@Override
	public CellState getCellState(int row, int column) {
		
		return currentGeneration.get(row, column);
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}

	}

	@Override
	public void step() {
		
		IGrid nextGeneration = currentGeneration.copy();
		for(int x = 0; x < nextGeneration.numRows(); x++) {
    		for(int y = 0; y < nextGeneration.numColumns(); y++) {
    			nextGeneration.set(x, y, getNextCell(x, y));
    			
    		}
    	}
		currentGeneration = nextGeneration;

	}

	@Override
	public CellState getNextCell(int row, int col) {
		
		CellState newCellState = CellState.DEAD;
		if(getCellState(row, col).equals(CellState.ALIVE)) {
			if(getCellState(row, col).equals(CellState.ALIVE)) {
				newCellState = CellState.DYING;
			}
			else if(getCellState(row, col).equals(CellState.DYING)) {
				newCellState = CellState.DEAD;
			}
		}
		if(getCellState(row, col).equals(CellState.DEAD)){
			if(countNeighbors(row, col, CellState.ALIVE) == 2) {
				newCellState = CellState.ALIVE;
			}
		}
		
		return newCellState;
	}

	@Override
	public int numberOfRows() {
		
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		
		return currentGeneration.numColumns();
	}
	
	private int countNeighbors(int row, int col, CellState state) {
		
		int counter = 0;
		for(int y = -1; y < 2; y++) {
			for(int x = -1; x < 2; x++) {
				if((row+x>=0)&&(col+y>=0)&&(row+x<numberOfRows())&&(col+y<numberOfColumns()) && !(x == 0 && y == 0)) {//passer på at den ikke sjekker CellState utenfor rutenettet
					if(getCellState(row+x, col+y).equals(state)) {
						counter++;
						
					}
				}
			}
		}
		return counter;
	}

	@Override
	public IGrid getGrid() {
		
		return currentGeneration;
	}

}
