package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	int cols;
	int rows;
	CellState[][] cellStateGrid;

    public CellGrid(int rows, int columns, CellState initialState) {
    	
    	this.rows = rows;
    	this.cols = columns;
    	this.cellStateGrid = new CellState[this.rows][this.cols];
    	for(int x = 0; x < rows; x++) {
    		for(int y = 0; y < columns; y++) {
    			cellStateGrid[x][y] = initialState;
    		}
    	}
    	
		// TODO Auto-generated constructor stub
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
    	cellStateGrid[row][column] = element;
        // TODO Auto-generated method stub
        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        return cellStateGrid[row][column];
    }

    @Override
    public IGrid copy() {
    	//CellState[][] gridCopy;
        IGrid newCellGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for(int x = 0; x < this.rows; x++) {
    		for(int y = 0; y < this.cols; y++) {
    			newCellGrid.set(x, y, this.get(x, y));
    		}
    	}
        
        return newCellGrid;
    }
    
}
